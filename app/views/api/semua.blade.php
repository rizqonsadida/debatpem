@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9">

        <ul class="nav nav-tabs" id="myTab">
          <!--li class="active"><a href="#profile" data-toggle="tab">Capres & Cawapres</a></li -->
          <li><a href="#berita" data-toggle="tab">Berita Pemilu</a></li>
          <li><a href="#event" data-toggle="tab">Event</a></li>
           <li><a href="#twit" data-toggle="tab">Cek Twit</a></li>
        </ul>
        
        <div class="tab-content">
<!-- tab kedua tentang capress -->        
          <div class="tab-pane active" id="profile">
          </hr>
          <div class="profile" id="capres-cawapress">
          </div>
          </div>
<!-- akhir capress -->
<!-- tab kedua tentang berita -->        
          <div class="tab-pane" id="berita">
            <h3>Berita Pemilu</h3>
              
    <hr/>
          <div class="row" id="news">
            </div>
          </div>
<!-- akhir berita -->

          <div class="tab-pane" id="event">
            <h3>Event Capres atau Cawapres</h3>
            <div id="debat">
            </div>
          </div>

          <div class="tab-pane" id="twit">
            <h3>Social Media Pemilu API</h3>
            <br>
            <div id="output">

            </div>
          </div>

        </div>
      </div>
  </div>
</div>
@stop

@section('js')
 <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>

<!-- script type="text/javascript">
$("#getTweets").bind("click", function() {
var twitterUsername = $("#twitterUsername").val();
var url = "https://twitter.com/status/user_timeline/" + twitterUsername + "?count=10&format=json&callback=?"
$.getJSON(url, function(data) {
    var twitterList = $("<ul />");
    $.each(data, function(index, item) {
        $("<li />", {
            "text": item.text
        }).appendTo(twitterList);
    });
    $("#output").fadeOut("fast", function() {
        $(this).empty().append(twitterList).fadeIn(3000);
    });
  })
});
</script -->

<script type="text/javascript">
$(document).ready(function() {
      $.ajax({
                    url: "http://api.pemiluapi.org/berita?json=get_recent_posts&apiKey=fea6f7d9ec0b31e256a673114792cb17",
                    dataType: "text",
                    success: function(data) {
                    var json = $.parseJSON(data);
                    var total = json.count;
            
                        for (var x = 0;x < total;x++){
                          $('#news').append('<div class="row"><div class="col-md-8"><div class="post-thumb"><div class="list-group-item"><h4 class="list-group-item-heading">' + json.posts[x].title + '</h4>'+json.posts[x].content+'</div></div></div></div></div>');
                        }
                    }
                });
      });
</script>
<!-- script type="text/javascript">
$(document).ready(function() {
        $.ajax({
                    url: "http://api.pemiluapi.org/calonpresiden/api/caleg?apiKey=fea6f7d9ec0b31e256a673114792cb17",
                    dataType: "text",
                    success: function(data) {
                    var json = $.parseJSON(data);
                    var total = json.data.results.count;
                    for (var x=0;x < total; x++){
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Nama</div><div class="col-md-4">'+json.data.results.caleg[x].nama+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Posisi</div><div class="col-md-4">'+json.data.results.caleg[x].role+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Jenis kelamin</div><div class="col-md-4">'+json.data.results.caleg[x].jenis_kelamin+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Agama</div><div class="col-md-4">'+json.data.results.caleg[x].agama+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Partai</div><div class="col-md-4">'+json.data.results.caleg[x].partai.nama+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append(
                        '<div class="row"><div class="col-md-2">Biografi</div><div class="col-md-12">'+json.data.results.caleg[x].biografi+'</div>' +
                        '</div>'
                        );
                      $('#capres-cawapress').append('</br>');
                    }
                  }
                });
      });
</script -->
<script type="text/javascript">
$(document).ready(function() {
      $.ajax({
                    url: "http://api.pemiluapi.org/socmedpemilu?apiKey=7b532e4991f255b768a77dbd68915939&r=post&perpage=10",
                    dataType: "text",
                    success: function(data) {
                    var json = $.parseJSON(data);
                    var total = 10;
            
                        for (var x = 0;x < total;x++){
                           $('#output').append(
                              '<div class="row"><div class="col-md-2"><img src="'+json.items[x].user.avatar_url+'" class="img-circle" width="80" height="80"></div><div class="col-md-8"><h4>'+json.items[x].user.name+'</h4><br/> ' + json.items[x].post.message +
                              
                              '</div>' +
                            '</div>'
                           );
                           $('#output').append('</br></br></br>');
                        }
                    }
                });
      });
</script>
<script type="text/javascript">
$(document).ready(function() {
      $.ajax({
                    url: "http://api.pemiluapi.org/calonpresiden/api/events?apiKey=fea6f7d9ec0b31e256a673114792cb17",
                    dataType: "text",
                    success: function(data) {
                    var json = $.parseJSON(data);
                    var total = json.data.results.count;
            
                        for (var x = 0;x < total;x++){
                           $('#debat').append(
                              '<div class="row"><div class="post-thumb col-md-9"><div class="list-group-item"><div class="row"><div class="col-md-6"><h4 class="list-group-item-heading">'+ json.data.results.events[x].judul +                              
                              '</h4></div><div class="col-md-3"><small>'+json.data.results.events[x].tanggal_mulai+'</small></div></div></div>'
                            

                            + '<div class="col-md-12">'+ json.data.results.events[x].deskripsi +                              
                            
                            '</div><br/><br/><br/></div></div>'
                           );
                           $('#debat').append('</br></br>');
                        }
                    }
                });
      });
</script>
@stop