		<div class="col-md-8">
			<legend>Trending Topic Today</legend>
			@foreach($post as $value)
			<div class="post-thumb">
				<div class="post-legend">
				<a href="{{URL::to($value->slug)}}" class="list-group-item">
				<h4 class="list-group-item-heading">{{$value->title}} <small>Started By {{$value->user->username}} at {{date("j, F, Y", strtotime($value->created_at))}}</small></h4>
				    <p>{{strip_tags(Str::limit($value->content,200))}}</p>
				</a>
				    <div class="row">
					    <div class="col-md-12">
					    	<div class="pull-right">
								<ul class="list-inline">
								  <li>{{$value->views}} View</li>
								  <li>{{Post::find($value->id)->comment()->count()}} Reply</li>
								  <?php $ikut = Post::find($value->id)->tagNames(); ?>
								  <li>
								  		@foreach($ikut as $key => $cot)
            						<a href="{{URL::to('tags/'.$cot)}}" class="label label-info">{{$cot}}</a>
        							@endforeach
								  </li>
								</ul>
							</div>
					    </div>						         
				     </div>
				</div>
			</div>
			@endforeach
			<div class="row text-center">
				{{$post->links()}}
			</div>
		</div>
		@if(Sentry::check())
		@include('rules.peraturan')
		<!-- AddThis Pro BEGIN -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535e539e54531802"></script>
		<!-- AddThis Pro END -->
		@endif
