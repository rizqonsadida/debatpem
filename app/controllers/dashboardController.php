<?php

class DashboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /dashboard
	 *
	 * @return Response
	 */
	public function getIndex(){
		$data['post'] = Post::whereHas('user', function($q){
			$q->orderBy('views');
		})->paginate(3);
		return View::make('belakang.index', $data);
	}

}