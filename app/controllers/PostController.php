<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$data['title'] = 'Bikin Perkara';
		return View::make('belakang.bikinPost', $data);
	}

	public function postCreatePost()
	{
		$post = new Post;
		$post->title = ucwords(Input::get('title'));
		$post->user_id = Sentry::getUser()->id;
		$post->content = Input::get('content');
		$post->up = 0;
		$post->down = 0;
		$post->views = 0;
		$post->save();
		$post->tag(Input::get('tags'));

		return Response::json(['slug' => $post->slug]);
	}

	public function postTanggapan()
	{
		$post_id = Input::get('postId');
		$user_id = Input::get('userId');
		$tanggapan = Input::get('tanggapan');

		if($tanggapan === 'pro'){
			$bg = '#2ecc71';
		}else{
			$bg = '#e74c3c';
		}

		$saya = DB::table('post_user')->insert(
		    array('post_id' =>  $post_id, 'user_id' => $user_id, 'tanggapan' => $tanggapan, 'color' => $bg)
		);

		return Response::json(array('status' => 'success'));
	}

	public function postComment()
	{
		$postid = Input::get('postId');
		$userid = Input::get('userId');
		$cont = Input::get('content');

		$comment = new Comment;
		$comment->post_id = $postid;
		$comment->user_id = $userid;
		$comment->slug = Crypt::encrypt($cont);
		$comment->content = $cont;
		$comment->save();

		$color =DB::table('post_user')
                ->where('post_user.post_id', '=', $postid)
                ->where('post_user.user_id', '=', $userid)
                ->select('post_user.color')
                ->get();

        $user = DB::table('post_user')->where('post_id', '=', $postid)->having('user_id', '=', $userid)->first();
		return Response::json(['username' => User::find($userid)->username, 'date' => date("j, F, Y"), 'content' => $cont, 'warna' => $user->color]);
	}

	public function postCreateSub(){

		$commentid = Input::get('commentId');
		$userid = Input::get('userId');
		$cont = Input::get('content');

		$data = DB::table('sub_comment')->insert([
			'comment_id' => $commentid,
			'user_id' => $userid,
			'content' => $cont
			]);
		return Response::json(['username' => User::find($userid)->username, 'date' => date("j, F, Y"), 'content' => $cont]);

	}

}