<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration {

	public function up()
	{
		Schema::create('post', function(Blueprint $table) {
			$table->increments('id')->unsigned;
			$table->timestamps();
			$table->string('title', 200);
			$table->string('slug', 255);
			$table->text('content');
			$table->integer('user_id')->unsigned();
			$table->integer('up');
			$table->integer('down');
			$table->integer('views');
		});
	}

	public function down()
	{
		Schema::drop('post');
	}
}